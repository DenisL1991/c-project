#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */
#define SIZE 12
int main(void) {
	int num[SIZE];
	int i,j,k;
	srand(5);
	printf("first:\n");
	for (i=0;i<SIZE-1;i++)
	{
		num[i]=rand()%21-10;
		printf("|%3d",num[i]);
	}
	printf("|\n");
	for (j=0;j<SIZE-1;j++)
	{
		for (i = 0; i<SIZE-j;i++)
		{
			if (num[i]>num[i+1])
			{
				k = num[i+1];
				num[i+1]=num[i];
				num[i]=k;
			}
		}
	}
	printf("second:\n");
	for(i=0;i<SIZE-1;i++)
	{
		printf("|%3d", num[i]);
	}
	printf("|\n");
	system("pause");
	return 0;
}
